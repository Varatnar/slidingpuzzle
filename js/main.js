//------------------------------//
//       CLASS DEFINITION       //
//------------------------------//

/**
 * Sliding puzzle game logic and data
 */
class SlidingPuzzle {
    /**
     *
     * @param sizeFactor {number}
     */
    constructor(sizeFactor) {
        /** @type {number} */
        this.sizeFactor = sizeFactor;
        /** @type {number} */
        this.nbrTiles = sizeFactor * sizeFactor - 1;
        /** @type {number} */
        this.consecutiveSquare = Math.sqrt(this.nbrTiles + 1);

        /** @type {Array<Tile>} */
        this.tiles = [];

        /** @type {Tile} */
        this.lastMovedTile = null;

        this._initGrid()
    }

    /**
     * Shuffling the puzzle by making 1000 valid random moves
     *
     * @return void
     */
    shuffle() {
        for (let i = 0; i < 1000; i++) {
            this.moveRandomTile();
        }
    }

    /**
     * Checks if the coordinate represent a square that is movable
     *
     * @param x {number} X coordinate
     * @param y {number} Y coordinate
     * @return {{x: number, y: number} | boolean}  the coordinate to move to or false if none
     */
    isSquareMovable(x, y) {

        if (y - 1 >= 0) {
            if (!this._retrieveTileFromCoordinate(x, y - 1)) {
                return {x: x, y: y - 1};
            }
        }

        if (y + 1 <= this.consecutiveSquare - 1) {
            if (!this._retrieveTileFromCoordinate(x, y + 1)) {
                return {x: x, y: y + 1};
            }
        }

        if (x - 1 >= 0) {
            if (!this._retrieveTileFromCoordinate(x - 1, y)) {
                return {x: x - 1, y: y};
            }
        }

        if (x + 1 <= this.consecutiveSquare - 1) {
            if (!this._retrieveTileFromCoordinate(x + 1, y)) {
                return {x: x + 1, y: y};
            }
        }

        return false;
    }

    /**
     * Move a random movable tile that wasn't moved the previous move.
     *
     * @returns {{current: {x: number, y: number}, tile: Tile}} information on movement (from, and state of moved tile)
     */
    moveRandomTile() {

        const potential = [];

        for (const tile of this.tiles) {
            if (this.isSquareMovable(tile.coordinate.x, tile.coordinate.y) &&
                (
                    !this.lastMovedTile ||
                    !(tile.coordinate.x === this.lastMovedTile.coordinate.x && tile.coordinate.y === this.lastMovedTile.coordinate.y)
                )) {
                // Using the object literal to break any links between reference and potential tile location data
                potential.push({
                    x: tile.coordinate.x,
                    y: tile.coordinate.y
                });
            }
        }

        const randomTile = potential[Math.floor(Math.random() * potential.length)];

        const movedTile = this.moveTile(randomTile.x, randomTile.y);

        return {
            current: {
                x: randomTile.x,
                y: randomTile.y
            },
            tile: movedTile
        }
    }

    /**
     * Given a set of coordinate, move the tile
     *
     * @param x {number} X coordinate of the tile
     * @param y {number} Y coordinate of the tile
     * @returns {Tile} The moved tile object.
     * @throws If the tile to be moved cannot be moved (a check prior to this method is needed in a normal flow)
     */
    moveTile(x, y) {

        if (!this.isSquareMovable(x, y)) {
            throw new Error(`Attempting to move unmovable tile [${x}, ${y}]`);
        }

        const tile = this._retrieveTileFromCoordinate(x, y);

        const newPosition = this.isSquareMovable(x, y);

        tile.coordinate.x = newPosition.x;
        tile.coordinate.y = newPosition.y;

        this.lastMovedTile = tile;

        return tile;
    }

    /**
     * @returns {boolean} True if game has been won.
     */
    hasWon() {
        for (const tile of this.tiles) {
            if (!tile.isAtRightPlace()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Change the game difficulty setting and re-init the tiles as well as the
     * different game settings.
     *
     * @param sizeFactor {number} New difficulty setting
     * @return {void}
     */
    changeDifficulty(sizeFactor) {
        this.sizeFactor = sizeFactor;

        this.nbrTiles = sizeFactor * sizeFactor - 1;
        this.consecutiveSquare = Math.sqrt(this.nbrTiles + 1);

        this._initGrid()
    }

    /**
     * Generate the different tiles for the game
     *
     * @return {void}
     * @private
     */
    _initGrid() {

        this.tiles = [];

        for (let i = 0; i < this.sizeFactor; i++) {
            for (let j = 0; j < this.sizeFactor; j++) {
                this.tiles.push(new Tile(i, j));
                if (this.tiles.length >= this.nbrTiles) {
                    break;
                }
            }

        }
    }

    /**
     * Retrieves the Tile object at a given coordinate
     *
     * @param x {number} X coordinate of potential tile
     * @param y {number} Y coordinate of potential tile
     * @return {Tile | null} Tile object at those coordinate, null if none
     * @private
     */
    _retrieveTileFromCoordinate(x, y) {
        for (const tile of this.tiles) {
            if (tile.coordinate.x === x && tile.coordinate.y === y) {
                return tile;
            }
        }

        return null;
    }
}

class Tile {
    /**
     * @param x {number}
     * @param y {number}
     */
    constructor(x, y) {
        /** @type {{sx: number, sy: number, x: number, y: number}} */
        this.coordinate = {
            sx: x, // sx standing for source x (original position / correct position)
            sy: y,
            x: x,
            y: y
        };
    }

    /**
     * @return {boolean}
     */
    isAtRightPlace() {
        return this.coordinate.sx === this.coordinate.x && this.coordinate.sy === this.coordinate.y;
    }

    equal(comparingTile) {
        if (this.coordinate.x !== comparingTile.coordinate.x) {
            return false
        }
        if (this.coordinate.y !== comparingTile.coordinate.y) {
            return false
        }
        if (this.coordinate.sx !== comparingTile.coordinate.sx) {
            return false
        }
        return this.coordinate.sy === comparingTile.coordinate.sy;
    }
}

/**
 * Graphical User Interface handler
 */
class GameArea {
    /**
     * @param canvas {HTMLCanvasElement} Canvas that will be used to represent the game.
     * @param game {SlidingPuzzle} Game handler object.
     */
    constructor(canvas, game) {
        /** @type {HTMLCanvasElement} */
        this.canvas = canvas;

        /** @type {CanvasRenderingContext2D} */
        this.context = canvas.getContext("2d");

        /** @type {HTMLImageElement} */
        this.gameImage = null;
        this._squareWidth = 0;
        this._squareHeight = 0;

        /** @type {AudioHack} */
        this.gameSound = null;
        this.isSoundOn = false;

        /** @type {boolean} */
        this.shuffling = false;
        /** @type {boolean} */
        this.showAnswer = false;

        /** @type {SlidingPuzzle} */
        this.game = game;

        this.tileMovementSpeedFactor = 10;

        /**
         * Click handler for canvas.
         */
        this.canvas.addEventListener("click", async (event) => {

            if (this.shuffling) {
                alert("Wait for the shuffling to end...");
                return;
            }

            if (this.game.hasWon()) {
                alert("You already won !\n Re-Shuffle if you want to play again.");
                return;
            }

            if (this.showAnswer) {
                this.draw();
                this.showAnswer = false;
                return;
            }

            const xClicked = event.pageX - this.canvas.offsetLeft;
            const yClicked = event.pageY - this.canvas.offsetTop;

            const xCoordinate = Math.floor(xClicked / this.canvas.width * this.game.consecutiveSquare);
            const yCoordinate = Math.floor(yClicked / this.canvas.height * this.game.consecutiveSquare);

            if (this.game.isSquareMovable(xCoordinate, yCoordinate)) {
                const movingTo = this.game.moveTile(xCoordinate, yCoordinate);
                await this.animateMovement(
                    xCoordinate * (this._squareWidth + BORDER_PIXEL),
                    yCoordinate * (this._squareHeight + BORDER_PIXEL),
                    movingTo);
                if (this.game.hasWon()) {
                    alert("You won !");
                }
            }
        });

    }

    /**
     * Shuffles the board while displaying each moves on the board.
     *
     * @return {Promise<void>}
     */
    async prettyShuffle() {
        for (let i = 0; i < this.game.sizeFactor * this.game.sizeFactor * this.game.sizeFactor; i++) {
            const movingTileData = this.game.moveRandomTile();
            await this.animateMovement(
                movingTileData.current.x * (this._squareWidth + BORDER_PIXEL),
                movingTileData.current.y * (this._squareHeight + BORDER_PIXEL),
                movingTileData.tile);
        }
    }

    /**
     * Moves a tile on the canvas image by image. (animation)
     *
     * @param currentX {number} Current X position of the tile
     * @param currentY {number} Current Y position of the tile
     * @param tile {Tile} Tile data, contains data of the tiles AFTER logical movement
     * @return {Promise<void>}
     */
    async animateMovement(currentX, currentY, tile) {

        return new Promise((resolve) => {

            // Retrieving destination canvas coordinates
            const destinationX = tile.coordinate.x * (this._squareWidth + BORDER_PIXEL);
            const destinationY = tile.coordinate.y * (this._squareHeight + BORDER_PIXEL);

            // Determining pixel that will be moved on this frame.
            let speedX = 0;
            let speedY = 0;

            if (currentX < destinationX) {
                speedX = this._squareWidth / this.tileMovementSpeedFactor;
                if (currentX + speedX > destinationX) {
                    speedX = destinationX - currentX;
                }
            } else {
                speedX = -this._squareWidth / this.tileMovementSpeedFactor;
                if (currentX + speedX < destinationX) {
                    speedX = destinationX - currentX;
                }
            }

            if (currentY < destinationY) {
                speedY = this._squareHeight / this.tileMovementSpeedFactor;
                if (currentY + speedY > destinationY) {
                    speedY = destinationY - currentY;
                }
            } else {
                speedY = -this._squareHeight / this.tileMovementSpeedFactor;
                if (currentY + speedY < destinationY) {
                    speedY = destinationY - currentY;
                }
            }

            // redrawing the whole canvas since redrawing only part of it might cause tearing due to fraction of pixel
            this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.context.fillStyle = "red";
            this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

            for (const gameTile of this.game.tiles.filter(element => !tile.equal(element))) {
                this._drawTile(gameTile.coordinate.sx, gameTile.coordinate.sy, gameTile.coordinate.x, gameTile.coordinate.y);
            }

            // drawing the moving tile (special drawing case, using context's drawImage directly
            this.context.drawImage(this.gameImage,
                tile.coordinate.sx * (this._squareWidth), tile.coordinate.sy * (this._squareHeight), this._squareWidth, this._squareHeight,
                currentX + speedX, currentY + speedY, this._squareWidth, this._squareHeight);

            // Checking if the animation is completed (tile where it should be)
            if (currentX + speedX !== destinationX || currentY + speedY !== destinationY) {
                window.requestAnimationFrame(async () => {
                    await this.animateMovement(currentX + speedX, currentY + speedY, tile);
                    resolve();
                });
            } else {
                if (this.isSoundOn) {
                    this.gameSound.play();
                }
                resolve();
            }
        });
    }

    /**
     * Given an src, loads an image
     *
     * @param src {string} Url of the image to load
     * @return {Promise<void>}
     */
    async loadImage(src) {
        return new Promise(((resolve, reject) => {
            this.gameImage = new Image();
            this.gameImage.src = src;

            this.gameImage.onload = () => {
                this.canvas.width = this.gameImage.width + (this.game.consecutiveSquare - 1) * BORDER_PIXEL;
                this.canvas.height = this.gameImage.height + (this.game.consecutiveSquare - 1) * BORDER_PIXEL;

                this._squareWidth = this.gameImage.width / this.game.consecutiveSquare;
                this._squareHeight = this.gameImage.height / this.game.consecutiveSquare;

                resolve();
            };
            this.gameImage.onerror = () => reject();
        }));
    }

    loadSound(src) {
        this.gameSound = new AudioHack(src);
    }

    /**
     * Draw all tiles of the current game on the canvas.
     *
     * @return {void}
     */
    draw() {

        // Update canvas size
        this.canvas.width = this.gameImage.width + (this.game.consecutiveSquare - 1) * BORDER_PIXEL;
        this.canvas.height = this.gameImage.height + (this.game.consecutiveSquare - 1) * BORDER_PIXEL;

        // Update square dimension
        this._squareWidth = this.gameImage.width / this.game.consecutiveSquare;
        this._squareHeight = this.gameImage.height / this.game.consecutiveSquare;

        if (!this.gameImage) {
            throw new Error("No image defined for the game");
        }

        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.fillStyle = "red";
        this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

        for (const tile of this.game.tiles) {
            this._drawTile(tile.coordinate.sx, tile.coordinate.sy, tile.coordinate.x, tile.coordinate.y)
        }

    }

    /**
     * Draw all the tiles at their appropriate location.
     * Help for players.
     *
     * @return {void}
     */
    drawAnswer() {
        if (!this.gameImage) {
            throw new Error("No image defined for the game");
        }

        if (this.showAnswer) {
            this.draw();
            this.showAnswer = false;
            return;
        }

        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.fillStyle = "red";
        this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

        for (const tile of this.game.tiles) {
            this._drawTile(tile.coordinate.sx, tile.coordinate.sy, tile.coordinate.sx, tile.coordinate.sy);
        }

        this.showAnswer = true;
    }

    /**
     * Given source coordinates and position coordinates, draws a tile.
     *
     * @param sx {number} Source X coordinate of tile.
     * @param sy {number} Source Y coordinate of tile.
     * @param x {number} Board X coordinate of tile.
     * @param y {number} Board Y coordinate of tile.
     * @return {void}
     * @private
     */
    _drawTile(sx, sy, x, y) {
        this.context.drawImage(this.gameImage,
            sx * (this._squareWidth), sy * (this._squareHeight), this._squareWidth, this._squareHeight,
            x * (this._squareWidth + BORDER_PIXEL), y * (this._squareHeight + BORDER_PIXEL), this._squareWidth, this._squareHeight);
    }
}

/**
 * Hack class to be able to use a sound multiple time before its completion.
 *
 * Using arbitrary stack size of 5
 */
class AudioHack {
    constructor(src) {

        this._audio = [];
        this._counter = 0;

        for (let i = 0; i < 5; i++) {
            const audio = new Audio(src);
            audio.volume = 0.3;
            this._audio.push(audio);
        }
    }

    /**
     * Play the next available sound
     */
    play() {
        this._audio[this._counter++ % this._audio.length].play();
    }
}

//----------------------------//
//       INITIALIZATION       //
//----------------------------//

const BORDER_PIXEL = 2;

const canvas = document.getElementById("slidingPuzzleArea");

const game = new SlidingPuzzle(3);
const gameArea = new GameArea(canvas, game);

// memory state of previously selected image value
let previousImageValue = document.getElementById("gameImage").value;

(async () => {
    gameArea.loadSound("./sound/sliding.mp3");
    await gameArea.loadImage("./img/apple.jpg");
    game.shuffle();
    gameArea.draw();
})();

//--------------------------//
//       PAGE ACTIONS       //
//--------------------------//

async function changeImage() { // eslint-disable-line no-unused-vars
    const selectElement = document.getElementById("gameImage");

    if (gameArea.shuffling) {
        selectElement.value = previousImageValue;
        alert("Wait for the shuffling to end...");
        return;
    }

    await gameArea.loadImage(`./img/${selectElement.value}.jpg`);
    previousImageValue = selectElement.value;
    gameArea.draw();
}

function changeDifficulty() { // eslint-disable-line no-unused-vars
    const selectElement = document.getElementById("gameDifficulty");

    if (gameArea.shuffling) {
        selectElement.value = game.sizeFactor;
        alert("Wait for the shuffling to end...");
        return;
    }

    game.changeDifficulty(selectElement.value);
    game.shuffle();
    gameArea.draw();
}

function reshuffle() { // eslint-disable-line no-unused-vars
    if (gameArea.shuffling) {
        alert("Wait for the shuffling to end...");
        return;
    }
    game.shuffle();
    gameArea.draw();
}

async function prettyReShuffle() { // eslint-disable-line no-unused-vars
    if (gameArea.shuffling) {
        alert("Already in the middle of shuffling the tiles");
        return;
    }
    gameArea.shuffling = true;
    await gameArea.prettyShuffle();
    gameArea.shuffling = false;
}

function showAnswer() { // eslint-disable-line no-unused-vars
    if (gameArea.shuffling) {
        alert("Wait for the shuffling to end...");
        return;
    }
    if (gameArea.showAnswer) {
        gameArea.draw();
        gameArea.showAnswer = false;
        return;
    }
    gameArea.drawAnswer();
}

function toggleSound() {// eslint-disable-line no-unused-vars
    const selectElement = document.getElementById("toggleSound");
    gameArea.isSoundOn = selectElement.checked;
}
